#include <stdio.h>
#include <limits.h>

int main(void) {

    int i = INT_MAX / 7;

    for(;i < INT_MAX - 1; i++) {
        int j = i * 7;

        if(j == 1333) {
            break;
        }
    }

    printf("FInished with i = %d\n", i);
}
