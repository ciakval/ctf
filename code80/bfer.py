#!/usr/bin/env python3

import hashlib
from telnetlib import Telnet
from datetime import datetime, timedelta

FLAG = ''

def hash(msg):
    return hashlib.sha1(bytes(msg, 'utf-8')).hexdigest()


def formats(dtime, letter):
    retval = []

    retval.append(dtime.strftime('%a %b %d %T CET %Y') + ':' + letter)
    retval.append(dtime.strftime('%a, %d %b %y %T +0100') + ':' + letter)
    retval.append(dtime.strftime('%T, 052th day of 2016') + ':' + letter)
    retval.append(dtime.strftime('%s') + ':' + letter)

    return retval


def tryAll(nowtime, hashValue):
    global FLAG
    deltas = range(-30, 31)

    letters = [ chr(x) for x in range(32, 128) ]
#    letters+= [ chr(x) for x in range(ord('a'), ord('z') + 1) ]


    for d in deltas:
        td = timedelta(seconds=d)

        thetime = nowtime + td

        for l in letters:

            for msg in formats(thetime, l):
                #print("Trying '" + msg + "'")

                msgHash = hash(msg)

                #print(msgHash)

                if msgHash == hashValue:
                    print("Found working time {0} and letter {1}".format(thetime, l))
                    print(msg)
                    FLAG += l
                    return msg



def main():
    global FLAG
    tn = Telnet('188.166.133.53', 11117)

    print(tn.read_until(b'\n'))

    while True:
        print(tn.read_until(b'\n'))

        splitline = tn.read_until(b'\n').decode('utf-8').split(' ')
        print(splitline)

        time = splitline[4]

        [ hours, minutes, seconds ] = time.split(':')

        seconds = seconds[:-1]

        nowtime = datetime(2016, 2, 21, int(hours), int(minutes), int(seconds))
        hashValue = splitline[16].rstrip()

        print(nowtime)
        print(hashValue)
        retval = tryAll(nowtime, hashValue)
        try:
            tn.write(bytes(retval, 'utf-8') + b'\r\n')
        except:
            print("Flag is " + FLAG)



if __name__ == "__main__":
    main()


