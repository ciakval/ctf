#!/usr/bin/env python3

from Tree import Tree
from telnetlib import Telnet

HOST = '188.166.133.53'
PORT = 11491

def main():
    tn = Telnet(HOST, PORT)

    while True:
        prompt = tn.read_until(b'\n').decode('utf-8')
        print("P: " + prompt)
        level = tn.read_until(b'\n').decode('utf-8')
        print("L: " + level)

        [ levcount, tree ] = level.split('.: ')

        tree = tree[1:-2]

        nums = [ int(x) for x in tree.split(',') ]

        print(nums)

        t = Tree(nums[0])
        for i in range(1, len(nums)):
            t.add(nums[i])

#        trees = [Tree(nums[0])]
#
#        index = 0
#
#        for i in range(1, len(nums)):
#            if trees[index].add(nums[i]) == False:
#                index = index + 1
#                trees.append(Tree(nums[i]))
#                print("Adding new tree with value {0}".format(nums[i]))
#
#        answer = []
#        for t in trees:
#            answer.extend(t.rev_preorder())
        
        answer = t.rev_preorder()
        print(answer)

        tn.write(bytes(str(answer), 'utf-8') + b'\r\n')

#        raw = b'['
#        for i in range(len(answer)):
#            raw = raw + bytes(str(answer[i]), 'utf-8')
#            if i < len(answer) - 1:
#                raw += b', '
#        raw += b']'
#
#        print(raw)
#        tn.write(raw + b'\r\n')
            
        #tn.write(bytes.encode(str(answer), 'utf-8') + b'\r\n')


if __name__ == "__main__":
    main()
