#!/usr/bin/env python3

class Tree:
    '''Binary tree class'''

    def __init__(self, val):
        self.left = None
        self.right = None
        self.parent = None

        self.value = val

    def add(self, val):
        #if self.value == val:
         #   return False
        if val <= self.value and self.left:
            return self.left.add(val)
        elif val > self.value and self.right:
            return self.right.add(val)
        else:
            self.add_child(val)
            return True



    def add_child(self, value):
        t = Tree(value)

        t.parent = self

        if value <= self.value:
            self.left = t
        else:
            self.right = t

    def __str__(self):
        retval = '('
        if self.left:
            retval += 'L: ' + str(self.left) + ','
        retval += str(self.value) + ','
        if self.right:
            retval += 'R: ' + str(self.right) + ','
        retval += ')'

        return retval

    def rev_preorder(self):
        retval = []

        retval.append(self.value)

        if self.right:
            retval.extend(self.right.rev_preorder())
        if self.left:
            retval.extend(self.left.rev_preorder())

        return retval



def main():
    VALS = [4, 17, 25, 11, 41]

    t = Tree(27)

    for v in VALS:
        t.add(v)

    print(t)

    print(t.rev_preorder())

if __name__ == "__main__":
    main()
