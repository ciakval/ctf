#!/usr/bin/env python3

LEVELS = [
        "4.4.5.3.3.3.3.3.3.3.6.4.3.3.3.3.3.4.4.5.3.3.3.3.3.4.6.4.3.3.3.3.3.4.3.5"
        ]


def decode(data):
    chars = []

    for i in range(0, len(data), 2):
        if len(chars) < i//8 + 1:
            chars.append([])
        chars[i//8].append(data[i])

    nums = []

    for char in chars:
        current = ''
        for c in char:
            if c == '3':
                current += '00'
            elif c == '4':
                current += '01'
            elif c == '5':
                current += '10'
            else:
                current += '11'

        nums.append(int(current, 2) ^ 32)
        
    eq = ''.join([ chr(x) for x in nums ])
    print(eq)

    return eq

def encode(num):
    s = str(num)

    chars = []

    for c in s:
        chars.append(c)

    nums = []
    for c in chars:
        nums.append(ord(c) ^ 32)

    result = ""

    for n in nums:
        first = n // 64
        result += str(first + 3)
        result += "."
        second = (n % 64) // 16
        result += str(second + 3)
        result += "."
        third = (n % 16) // 4
        result += str(third + 3)
        result += "."
        fourth = (n % 4)
        result += str(fourth + 3)
        result += "."

    result = result[:-1]
    return result


#encode(decode(LEVELS[0]))

