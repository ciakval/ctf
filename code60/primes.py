#!/usr/bin/env python3

import re
from telnetlib import Telnet

def nextprime(num):
    i = num

    ok = False

    while True:
        i = i + 1

        ok = True
        q = 2
        while q < i:
            if i%q == 0:
                ok = False
                break
            q = q + 1

        if ok:
            return i



def calc(line):
    print("Calc: working on this: " + line)
#    m = re.compile(r'^Level \d+\.: (?P<first>x|-?\d+) (?P<op>[+*/-]) (?P<second>x|-?\d+) = (?P<result>x|-?\d+)$')

    m = re.compile(r'^Level \d+\.: Find the next prime number after (?P<num>\d+):$')

    s = m.search(line)

    return nextprime(int(s.group('num')))


def run():
    tn = Telnet('188.166.133.53', 11059)

    while True:
        prompt = tn.read_until(b'\n').decode('utf-8')
        print(prompt)

        example = tn.read_until(b'\n').decode('utf-8')
        
        print("Next example: " + example)
        result = calc(example)
        tn.write(bytes(str(result), 'utf-8') + b'\r\n')



run()
