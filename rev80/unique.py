#!/usr/bin/env python3

import sys

printed = []

for c in sys.argv[1]:
    if c not in printed:
        printed.append(c)
        print(c)
