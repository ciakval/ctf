#!/usr/bin/env python3

# File:     Cube.py
# Author:   Honza Remes (jan.remes@hig.no)
# 
# Description:

class Cube:
    '''Class for Rubik cube's elements'''

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        
        self.orig = [x,y,z]

        self.faces = {
                'F': '_',
                'B': '_',
                'U': '_',
                'D': '_',
                'L': '_',
                'R': '_'
                }

    def __str__(self):
        return "Cube at [{0},{1},{2}]: ".format(self.x,self.y,self.z) + str(self.faces)
        
    def face(self, face):
        return self.faces[face]

    def rotate(self, axis):
        '''Transform the tile - apply 90 degree clockwise rotation in the specified axis'''
        if axis == 'x':
            tmp = self.y
            self.y = self.z
            self.z = 2-tmp

            tmp = self.faces['U']
            self.faces['U'] = self.faces['B']
            self.faces['B'] = self.faces['D']
            self.faces['D'] = self.faces['F']
            self.faces['F'] = tmp

        elif axis == 'y':
            tmp = self.z
            self.z = self.x
            self.x = 2-tmp
            
            tmp = self.faces['F']
            self.faces['F'] = self.faces['L']
            self.faces['L'] = self.faces['B']
            self.faces['B'] = self.faces['R']
            self.faces['R'] = tmp

        elif axis == 'z':
            tmp = self.x
            self.x = self.y
            self.y = 2-tmp

            tmp = self.faces['U']
            self.faces['U'] = self.faces['L']
            self.faces['L'] = self.faces['D']
            self.faces['D'] = self.faces['R']
            self.faces['R'] = tmp

        else:
            return False

        return True

    def cubmove(self, move):
        if move == 'F' and self.z == 0:
            self.rotate('z')
        elif move == 'B' and self.z == 2:
            self.rotate('z')
            self.rotate('z')
            self.rotate('z')
        elif move == 'U' and self.y == 2:
            self.rotate('y')
            self.rotate('y')
            self.rotate('y')
        elif move == 'D' and self.y == 0:
            self.rotate('y')
        elif move == 'L' and self.x == 0:
            self.rotate('x')
        elif move == 'R' and self.x == 2:
            self.rotate('x')
            self.rotate('x')
            self.rotate('x')


class Rubik:
    def __init__(self):
        self.cubes = []

        for x in [0,1,2]:
            for y in [0,1,2]:
                for z in [0,1,2]:
                    self.cubes.append(Cube(x,y,z))

        for c in self.cubes:
            if c.z == 0:
                c.faces['F'] = 'f'
            elif c.z == 2:
                c.faces['B'] = 'b'

            if c.x == 0:
                c.faces['L'] = 'l'
            elif c.x == 2:
                c.faces['R'] = 'r'

            if c.y == 0:
                c.faces['D'] = 'd'
            elif c.y == 2:
                c.faces['U'] = 'u'

    def toString(self, face):
        default = [0,1,2]
        X = default
        Y = default
        Z = default

        if face == 'F':
            Z = [0]
        elif face == 'B':
            Z = [2]
        elif face == 'U':
            Y = [2]
        elif face == 'D':
            Y = [0]
        elif face == 'L':
            X = [0]
        elif face == 'R':
            X = [2]
        else:
            return "Invalid face"

        retval = "Rubik({0}): ".format(face)

        chosen = {}
        
        for c in self.cubes:
            if c.x in X and c.y in Y and c.z in Z:
                chosen[c.y * 100 + c.z * 10 + c.x] = c
                
        keys = list(chosen.keys())
        keys.sort()
        
        for k in keys:
            retval += chosen[k].face(face)
            retval += " "

        return retval

    def move(self, move):
        for c in self.cubes:
            c.cubmove(move)




def solve(rubik, moves):
    for m in moves:
        rubik.move(m)
        
    return rubik

def main():
    moves = list('FFFDDDUUULLLUUUFFBBDDFFFUDDBBBUUUBBRRDDBBBRRRUBBLURRRUUULLL')
    r = Rubik()

    for f in ['F', 'B', 'L', 'R', 'U', 'D']:
        print(r.toString(f))

    print('\n\n')

    for m in moves:
        r.move(m)

    for f in ['F', 'B', 'L', 'R', 'U', 'D']:
        print(r.toString(f))