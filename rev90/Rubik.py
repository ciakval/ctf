#!/usr/bin/env python3

# File:     Rubik.py
# Author:   Honza Remes (jan.remes@hig.no)
# 
# Description:

class Cube:
    '''Rubik's cube element class'''

    def __init__(self, orientation='F'):
        self.orientation = orientation
        self.faces = {
                'F' : 'a',
                'B' : '',
                'U' : '',
                'D' : '',
                'R' : '',
                'L' : ''
        }

    def __str__(self):
        return "Cube({0}): ".format(self.orientation) + str(self.faces)

    def __repr__(self):
        return str(self)

    def face(self, f):
        return self.faces[f]

    def set(self, face, val):
        self.faces[face] = val

class Rubik:
    '''Class representing Rubik cube'''

    def __init__(self):
        self.data = [
                [
                    [ Cube(), Cube(), Cube() ],
                    [ Cube(), Cube(), Cube() ],
                    [ Cube(), Cube(), Cube() ],
                ],
                [
                    [ Cube(), Cube(), Cube() ],
                    [ Cube(), Cube(), Cube() ],
                    [ Cube(), Cube(), Cube() ],
                ],
                [
                    [ Cube(), Cube(), Cube() ],
                    [ Cube(), Cube(), Cube() ],
                    [ Cube(), Cube(), Cube() ],
                ],
            ]

        [ self.data[x][y][z].set('F','W') for x in [0] for y in [0,1,2] for z in [0,1,2] ]
        [ self.data[x][y][z].set('B','B') for x in [2] for y in [0,1,2] for z in [0,1,2] ]

        [ self.data[x][y][z].set('L','Y') for x in [0,1,2] for y in [0,1,2] for z in [0] ]
        [ self.data[x][y][z].set('R','G') for x in [0,1,2] for y in [0,1,2] for z in [2] ]

        [ self.data[x][y][z].set('U','R') for x in [0,1,2] for y in [0] for z in [0,1,2] ]
        [ self.data[x][y][z].set('D','O') for x in [0,1,2] for y in [2] for z in [0,1,2] ]

        self.orientation = 'F'

    def __str__(self):
        default = [ 0, 1, 2 ]
        first = default
        second = default
        third = default

        if self.orientation == 'F':
            first = [ 0 ]
        elif self.orientation == 'B':
            first = [ 2 ]
        elif self.orientation == 'U':
            second = [ 0 ]
        elif self.orientation == 'D':
            second = [ 2 ]
        elif self.orientation == 'L':
            third = [ 0 ]
        elif self.orientation == 'R':
            third = [ 2 ]
        else:
            return "Rubik: Invalid orientation"

        retval = "Rubik:\n"

        for x in first:
            for y in second:
                for z in third:
                    retval += self.data[x][y][z].face(self.orientation)
                    retval += ' '

        return retval

    def move(self, m):
        if m == 'F':
            tmp = self.data[0][0][0]
            self.data[0][0][0] = self.data[0][2][0]
            self.data[0][2][0] = self.data[0][2][2]
            self.data[0][2][2] = self.data[0][0][2]
            self.data[0][0][2] = tmp




def main():
    r = Rubik()
    r.orientation = 'U'
    print(r)

    print(r.data)


if __name__ == "__main__":
    main()
