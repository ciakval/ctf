#!/usr/bin/env python3

from telnetlib import Telnet

def main():
    tn = Telnet('188.166.133.53', 12037)

    print(tn.read_until(b'\n'))

    tn.write(b'ffffffffff\nffff')

    print(tn.read_until(b'\n'))
    print(tn.read_until(b'\n'))


if __name__ == "__main__":
    main()
