#!/usr/bin/env python3

import sys

args = ""
for i in range(1, len(sys.argv)):
    args += sys.argv[i]
    args += " "

c1 = list(args)

c = [ x * 2 for x in reversed(c1) ]

print(''.join(c))
