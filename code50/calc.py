#!/usr/bin/env python3

import re
from telnetlib import Telnet

def calc(line):
    print("Calc: working on this: " + line)
    m = re.compile(r'^Level \d+\.: (?P<first>x|-?\d+) (?P<op>[+*/-]) (?P<second>x|-?\d+) = (?P<result>x|-?\d+)$')

    s = m.search(line)

    if s.group('result') == "x":
        if s.group('op') == "+":
            result = int(s.group('first')) + int(s.group('second'))
        elif s.group('op') == "-":
            result = int(s.group('first')) - int(s.group('second'))
        elif s.group('op') == "*":
            result = int(s.group('first')) * int(s.group('second'))
        elif s.group('op') == "/":
            result = int(s.group('first')) / int(s.group('second'))
        else:
            result = None
    else:
        if s.group('first') == "x":
            other = int(s.group('second'))
            first = True
        else:
            other = int(s.group('first'))
            first = False

        if s.group('op') == "+":
            result = int(s.group('result')) - other
        elif s.group('op') == "-":
            if first:
                result = int(s.group('result')) + other
            else:
                result = other - int(s.group('result'))
        elif s.group('op') == "*":
            result = int(s.group('result')) / other
        elif s.group('op') == "/":
            if first:
                result = int(s.group('result')) * other
            else:
                result = other / int(s.group('result'))
        else:
            result = None

    print("Result is {0}".format(result))

    return int(result)

def run():
    tn = Telnet('188.166.133.53', 11027)

    while True:
        prompt = tn.read_until(b'\n').decode('utf-8')
        print(prompt)

        example = tn.read_until(b'\n').decode('utf-8')
        
        print("Next example: " + example)
        result = calc(example)
        tn.write(bytes(str(result), 'utf-8') + b'\r\n')



calc("Level 3.: x + 3 = 5")
calc("Level 5.: 10 - x = 2")
calc("Level 7.: -10 - x = 2")
calc("Level 9.: -10 - 3 = x")

run()
